#! /bin/bash

set -euxo pipefail



function main() {
    local build="$( mktemp --directory --tmpdir=. build.XXXXXXXXXX )"
    local venv="$( mktemp --directory --tmpdir=. venv.XXXXXXXXXX )"
    python3 -m venv "$venv"

    "$venv"/bin/pip install --upgrade pip
    "$venv"/bin/pip install --no-compile wheel
    "$venv"/bin/pip install --no-compile -r requirements.txt
    "$venv"/bin/pip install --no-compile .
    "$venv"/bin/pip uninstall -y setuptools pkg_resources pip wheel
    cp -a "$venv"/lib/python3.*/site-packages/* "$build"/

    ( cd "$build"; zip -r - . ) >lambda.zip

    rm -rf "$build" "$venv"
}



if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
    main
fi

