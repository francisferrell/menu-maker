#!/bin/bash

set -euxo pipefail

aws lambda update-function-code --function-name $1 --zip-file fileb://lambda.zip

