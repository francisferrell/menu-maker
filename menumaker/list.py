
import asyncio
from datetime import date, timedelta
from os import environ

import boto3
from ourgroceries import OurGroceries

from . import g
from .common import find_next_date, Recipe, ingredients_from_text

g.DocumentKlass = Recipe

DO_NOT_BUY = set( [
    'salt',
    'pepper',
    'salt and pepper',
    'kosher salt',
    'water',
] )

INGREDIENT_MAP = {
    'egg yolks': 'eggs',
    'lemon juice': 'lemon',
    'lemon zest': 'lemon',
}



def ourgroceries_login():
    ssm = boto3.client( 'ssm' )
    path = '/menu-maker/ourgroceries/'
    response = ssm.get_parameters_by_path( Path = path, WithDecryption = True )
    config = { param['Name'][len(path):]: param['Value'] for param in response['Parameters'] }
    og = OurGroceries( config.pop( 'username' ), config.pop( 'password' ) )
    asyncio.run( og.login() )
    return og, config



def ourgroceries_find_list_id( lists, name ):
    for item in lists['shoppingLists']:
        if item['name'] == name:
            return item['id']



# fortunately ourgroceries will auto-assign the last category used for an item
# unfortunately, it strictly matches last usage of an item on *both* the item name and notes
# so if we want to add "chicken" with note "for recipe X", it will only remember the category
# for that combiantion and the first time we add that combination to the list, it won't auto
# assign the last category that "chicken" without a note had.
# but the master list shows us all the item history, so we can just do the more relaxed search
# through that history ourselves and, when there isn't an exact match, look up the last usage
# with no note
def ourgroceries_get_category_for_item( master_list, name, recipe ):
    best_candidate = None
    candidate = None

    # every item has a value
    # some items have notes
    # some items have categories
    # we want the category for the item that matches on value and note
    # if no such match, we want the category for the item that matches on value
    # if neither, give up, there is no already-defined category
    for item in master_list:
        if item.get( 'value' ) == name and item.get( 'note' ) == recipe and item.get( 'categoryId' ) is not None:
            best_candidate = item
        elif item.get( 'value' ) == name and item.get( 'categoryId' ) is not None:
            candidate = item

    if best_candidate is not None:
        return best_candidate.get( 'categoryId' )

    if candidate is not None:
        return candidate.get( 'categoryId' )



def create_list( calendar_name, origin_date, list_name, dry_run, days = 6 ):
    print( f"making a 7 day shopping list starting {origin_date:%a %b %d %Y}" )
    print( f"Using calendar {calendar_name}" )
    if dry_run:
        print( "Drafting it for review" )
    else:
        print( "Putting it on the list" )

    print()
    print( 'looking at the calendar...' )
    calendar = g.Calendar( calendar_name )
    scheduled = []
    meals = []

    for e in calendar.get_events( origin_date, origin_date + timedelta( days = days ) ):
        print( f'    found {e.summary}' )
        if not e.is_all_day:
            continue

        meals.append( e )
        scheduled.extend( e.attachments )

    print()
    print( 'looking at the recipes...' )
    for recipe in scheduled:
        print( f'    {recipe.title}' )

    print()
    print( 'gathering ingredients...' )
    list_ingredients = list()
    already_added = set()

    for recipe in scheduled:
        for ingredient in recipe.ingredients:
            if ':' not in ingredient:
                continue
            if ingredient.endswith( ':' ):
                continue

            ingredient_name = ingredient.split( ':' )[0].lower()
            ingredient_name = INGREDIENT_MAP.get( ingredient_name, ingredient_name )

            if ingredient_name in DO_NOT_BUY:
                print( f'    skipping {ingredient_name} on the do not buy list' )
            elif ( ingredient_name, recipe.title ) in already_added:
                print( f'    skipping duplicate {ingredient_name} {recipe.title}' )
            else:
                list_ingredients.append( dict (
                    recipe = recipe.title,
                    ingredient = ingredient_name,
                ) )
                already_added.add( ( ingredient_name, recipe.title ) )

    for meal in meals:
        if meal.description is None:
            continue

        for ingredient in ingredients_from_text( meal.description, keywords = [ 'ingredients', 'groceries', 'list maker', 'shopping list' ] ):
            recipe_title = meal.summary
            ingredient_name = ingredient.lower()
            ingredient_name = INGREDIENT_MAP.get( ingredient_name, ingredient_name )

            if ( ingredient_name, recipe_title ) in already_added:
                print( f'    skipping duplicate {ingredient_name} {recipe_title}' )
            else:
                list_ingredients.append( dict (
                    recipe = recipe_title,
                    ingredient = ingredient_name,
                ) )
                already_added.add( ( ingredient_name, recipe_title ) )

    og, og_config = ourgroceries_login()
    og_lists = asyncio.run( og.get_my_lists() )
    master_list = asyncio.run( og.get_list_items( og_lists['masterListId'] ) )['list']['items']
    this_list_id = ourgroceries_find_list_id( og_lists, list_name )

    print()
    print( f'adding items to {list_name}...' )
    for item in list_ingredients:
        item_value = item['ingredient']
        item_recipe = item['recipe']
        item_note = f'for {item_recipe}'
        extra_args = dict( note = item_note )
        category_id = ourgroceries_get_category_for_item( master_list, item_value, item_note )

        if category_id is not None:
            extra_args.update( category = category_id )
        else:
            extra_args.update( auto_category = True )

        print( f'    adding {item_value} {extra_args["note"]}' )
        if not dry_run:
            asyncio.run( og.add_item_to_list( this_list_id, item_value, **extra_args ) )



def lambda_handler( event, context ):
    args = dict(
        calendar_name = environ.get( 'CALENDAR_NAME', 'menu' ),
        list_name = environ.get( 'LIST', 'Giant' ),
        origin = environ.get( 'ORIGIN', 'sun' ),
        dry_run = False,
    )

    args.update( event )
    args['origin_date'] = find_next_date( args.pop( 'origin' ) )

    create_list( **args )



def cli_main():
    from argparse import ArgumentParser

    parser = ArgumentParser( description = "Automatically plan the shopping list based on next week's menu" )
    parser.add_argument(
        '--calendar', '-c',
        default = 'menu-maker',
        help = 'Google Calendar name to read events from'
    )
    parser.add_argument(
        '--list', '-l',
        default = 'Menu Maker',
        help = 'the OurGroceries list to add items to'
    )
    parser.add_argument(
        '--origin', '-o',
        default = 'sun',
        help = 'the first day of the menu planning week'
    )
    parser.add_argument(
        '--days', '-d',
        default = 6,
        type = int,
        help = 'the number of days to pull from the calendar'
    )
    parser.add_argument(
        '--dry-run', '-n',
        action = 'store_true',
        help = "only write the planned list to stdout, don't create list entries"
    )

    args = parser.parse_args()

    calendar_name = args.calendar
    origin_date = find_next_date( args.origin )
    list_name = args.list
    dry_run = args.dry_run

    create_list( calendar_name, origin_date, list_name, dry_run, days = args.days )

