
from datetime import date, timedelta
from os import environ
from random import choice, shuffle

from . import g
from .common import find_next_date, weekdays, Recipe, ONE_DAY, ONE_WEEK, THREE_WEEKS

g.DocumentKlass = Recipe



def get_available_dates( origin_date, num_days, already_scheduled ):
    available_dates = [ origin_date ]
    while len( available_dates ) < num_days:
        available_dates.append( available_dates[-1] + ONE_DAY )

    return sorted( list( set( available_dates ) - { e.start for e in already_scheduled } ) )



def get_available_recipes( recipes, already_scheduled ):
    already_scheduled_recipes = set()
    for e in already_scheduled:
        already_scheduled_recipes.update( e.attachments )

    return list( recipes - already_scheduled_recipes )



class RecipeBook:

    def __init__( self, recipes ):
        self.recipes = {
            1: [],
            2: [],
            3: [],
        }

        for recipe in recipes:
            self.recipes[recipe.effort].append( recipe )


    def pick( self, effort ):
        try:
            recipe = choice( self.recipes[effort] )
        except IndexError:
            print( f"failed to find a recipe of effort {effort} in {self.recipes!r}" )
            return None

        self.recipes[effort].remove( recipe )
        return recipe



def create_plan( root_folder_name, entrees_folder_name, soups_stew_folder_name, calendar_name, origin_date, days_to_plan, dry_run ):
    print( f"Planning a {days_to_plan} day menu starting {origin_date:%a %b %d %Y}" )
    print( f"Using calendar {calendar_name}" )
    print( f"Drawing from {root_folder_name} {entrees_folder_name!r} and {soups_stew_folder_name!r}" )
    if dry_run:
        print( "Drafting it for review" )
    else:
        print( "Putting it on the calendar" )

    print()
    print( 'finding drive...' )
    root_folder = g.Folder( root_folder_name )

    entrees_folder = root_folder.folders[entrees_folder_name]
    all_entrees = set( entrees_folder.documents )

    soups_stew_folder = root_folder.folders[soups_stew_folder_name]
    all_soups_stew = set( soups_stew_folder.documents )

    print( 'looking at the calendar...' )
    calendar = g.Calendar( calendar_name )
    already_scheduled = []

    for e in calendar.get_events( origin_date - THREE_WEEKS, origin_date + ONE_WEEK ):
        if e.is_all_day and len( e.attachments ) > 0:
            already_scheduled.append( e )

    print( 'sorting the recipes...' )
    available_entrees = RecipeBook( get_available_recipes( all_entrees, already_scheduled ) )
    #available_soups_stew = RecipeBook( get_available_recipes( all_soups_stew, already_scheduled ) )
    print( 'planning...' )
    dates_to_plan = get_available_dates( origin_date, days_to_plan, already_scheduled )
    events = []
    # Sunday is statically allocated as a 3
    # The remaining days need to be randomly allocated as 2s or 1s with at most 2 2s.
    # so set a quota of 2 2s and fill in the rest 1s, however many that migt be
    effort_quota = [ 2, 2 ] + ( [ 1 ] * ( days_to_plan - 3 ) )
    shuffle( effort_quota )

    for date in dates_to_plan:
        if date.weekday() == weekdays['sun']:
            recipe = available_entrees.pick( 3 )
        else:
            effort = effort_quota.pop()
            recipe = available_entrees.pick( effort )

        if recipe is None:
            continue

        events.append( dict(
            start = date,
            summary = recipe.title,
            attachments = [ recipe ],
        ) )

    for recipe in [ event['attachments'][0] for event in events ]:
        if recipe.includes( 'rice' ):
            events.append( dict(
                start = origin_date,
                summary = 'make rice'
            ) )
        if recipe.includes( 'boiled egg' ):
            events.append( dict(
                start = origin_date,
                summary = 'boil eggs'
            ) )

    print()
    for event in events:
        if 'attachments' in event:
            print( f"{event['start']:%a %b %d}: {event['summary']} ({event['attachments'][0].effort})" )
        else:
            print( f"{event['start']:%a %b %d}: {event['summary']}" )

        if not dry_run:
            calendar.add_event( **event )



def lambda_handler( event, context ):
    args = dict(
        root_folder_name = environ.get( 'RECIPES_FOLDER_NAME', 'Recipes' ),
        entrees_folder_name = environ.get( 'ENTREES_FOLDER_NAME', 'Entrees' ),
        soups_stew_folder_name = environ.get( 'SOUPS_AND_STEWS_FOLDER_NAME', 'Soups and Stews' ),
        calendar_name = environ.get( 'CALENDAR_NAME', 'menu' ),
        origin = environ.get( 'ORIGIN', 'sun' ),
        days_to_plan = int( environ.get( 'DAYS_TO_PLAN', 5 ) ),
        dry_run = False,
    )

    args.update( event )
    args['origin_date'] = find_next_date( args.pop( 'origin' ) )

    create_plan( **args )



def cli_main():
    from argparse import ArgumentParser

    parser = ArgumentParser( description = "Automatically plan next week's menu" )
    parser.add_argument(
        '--folder', '-f',
        default = 'Recipes',
        help = 'Google Drive root folder name'
    )
    parser.add_argument(
        '--entrees', '-e',
        default = 'Entrees',
        help = 'Google Drive containing entrees, must be a child of the root folder'
    )
    parser.add_argument(
        '--stews', '-s',
        default = 'Soups and Stews',
        help = 'Google Drive containing cold weather recipes, must be a child of the root folder'
    )
    parser.add_argument(
        '--calendar', '-c',
        default = 'menu-maker',
        help = 'Google Calendar name to add events to '
    )
    parser.add_argument(
        '--origin', '-o',
        default = 'sun',
        help = 'the first day of the menu planning week'
    )
    parser.add_argument(
        '--days', '-d',
        type = int,
        default = 5,
        help = 'the number of days to plan'
    )
    parser.add_argument(
        '--dry-run', '-n',
        action = 'store_true',
        help = "only write the planned menu to stdout, don't create calendar entries"
    )

    args = parser.parse_args()

    root_folder_name = args.folder
    entrees_folder_name = args.entrees
    soups_stew_folder_name = args.stews
    calendar_name = args.calendar
    origin_date = find_next_date( args.origin )
    days_to_plan = args.days
    dry_run = args.dry_run

    create_plan( root_folder_name, entrees_folder_name, soups_stew_folder_name, calendar_name, origin_date, days_to_plan, dry_run )



if __name__ == '__main__':
    cli_main()

