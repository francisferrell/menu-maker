
from datetime import date, timedelta
import re

from . import g

ONE_DAY = timedelta( days = 1 )
ONE_WEEK = timedelta( days = 7 )
THREE_WEEKS = timedelta( days = 21 )



weekdays = dict(
    mon = 0,
    monday = 0,
    tue = 1,
    tuesday = 1,
    wed = 2,
    wednesday = 2,
    thu = 3,
    thursday = 3,
    fri = 4,
    friday = 4,
    sat = 5,
    saturday = 5,
    sun = 6,
    sunday = 6,
)

def find_next_date( weekday, origin = None ):
    if origin is None:
        origin = date.today()

    diff = weekdays[weekday.lower()] - origin.weekday()
    if diff <= 0:
        diff += 7
    return origin + timedelta( days = diff )



class Recipe( g.Document ):

    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        self.reload()


    def __repr__( self ):
        return f'<Recipe {self.title!r}>'


    def reload( self ):
        self._text = None
        self._ingredients = None
        self._effort = None


    @property
    def text( self ):
        if self._text is None:
            self._text = g.Folder.service.files().export(
                fileId = self.id,
                mimeType = 'text/plain',
            ).execute().decode( 'utf-8-sig' ).replace( '\r\n', '\n' )

        return self._text


    @property
    def effort( self ):
        if self._effort is None:
            lines = iter(self.text.splitlines())
            for line in lines:
                if line == 'Effort':
                    break

            try:
                line = next( lines )
            except StopIteration:
                line = '2'

            try:
                self._effort = int( line )
            except ValueError:
                self._effort = 2

        return self._effort


    @property
    def ingredients( self ):
        if self._ingredients is None:
            self._ingredients = list( ingredients_from_text( self.text ) )

        return self._ingredients


    def includes( self, candidate ):
        candidate = candidate.lower()

        for ingredient in self.ingredients:
            if candidate in ingredient.lower():
                return True

        return False



def ingredients_from_text( text, keywords = [ 'ingredients' ] ):
    all_keywords = [ s.lower() for s in keywords ]
    all_keywords.extend( [ f'{s.lower()}:' for s in keywords ] )

    if text.startswith( '<html-blob>' ):
        lines = iter( [ re.sub( r'<[^>]+>', '', line ) for line in text.split( '<br>' ) ] )
    else:
        lines = iter( text.splitlines() )

    for line in lines:
        line = line.lower()
        if line in all_keywords:
            break

    try:
        while True:
            line = next( lines )
            line = line.strip()
            if line.startswith( '*' ) or line.startswith( '-' ):
                yield line[1:].strip()
            else:
                break
    except StopIteration:
        pass

