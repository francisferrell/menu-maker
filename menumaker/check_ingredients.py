
from itertools import chain

import g
from main import Recipe


g.DocumentKlass = Recipe
folders = [
    g.Folder('Entrees'),
    g.Folder('Breakfast'),
    g.Folder('Desserts'),
    g.Folder('Other'),
    g.Folder('Sides'),
    g.Folder('Soups and Stews'),
    g.Folder('Specials'),
]

recipes = list( chain( * [ f.documents for f in folders ] ) )
all_ingredients = chain( * [ r.ingredients for r in recipes ] )
list_ingredients = set()
non_ingredients = set()

for ingredient in all_ingredients:
    if ':' in ingredient and not ingredient.endswith( ':' ):
        list_ingredients.add( ingredient.split( ':' )[0].lower() )
    else:
        non_ingredients.add( ingredient.lower() )

print( '==== ingredients ====' )
for i in sorted( list( list_ingredients ) ):
    print( i )

print()
print( '==== everything else ====' )
for i in sorted( list( non_ingredients ) ):
    print( i )

print()
print( f'totals: {len(list_ingredients)} / {len(non_ingredients)} from {len( recipes )} recipes' )

print()
print( 'recipes with no ingredients:' )
for recipe in recipes:
    if len( recipe.ingredients ) == 0:
        print( recipe.title )

