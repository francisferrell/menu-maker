
from datetime import date, datetime, time, timedelta
import json
from os import environ

import boto3
from google.oauth2.service_account import Credentials
from googleapiclient.discovery import build
import googleapiclient.errors
from pytz import timezone

ONE_DAY = timedelta( days = 1 )
START_OF_DAY = time( 0, 0, 0 )
END_OF_DAY = time( 23, 59, 59 )



SecretsManager = boto3.client( 'secretsmanager' )

secret = SecretsManager.get_secret_value( SecretId = environ['GOOGLE_API_CREDENTIALS_ARN'] )
secret = json.loads( secret['SecretString'] )

google_credentials = Credentials.from_service_account_info(
    secret,
    scopes = [
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/calendar.readonly',
        'https://www.googleapis.com/auth/calendar.events',
        'https://www.googleapis.com/auth/drive.readonly',
        'https://www.googleapis.com/auth/drive.metadata.readonly',
    ]
)

secret = None



class Calendar:

    service = build( 'calendar', 'v3', credentials = google_credentials, cache_discovery = False )

    class NotFoundError( Exception ):
        pass


    def __init__( self, expected_name ):
        results = []
        pageToken = None

        while True:
            this_results = Calendar.service.calendarList().list( pageToken = pageToken ).execute()
            results.extend( this_results['items'] )

            try:
                pageToken = this_results['nextPageToken']
            except KeyError:
                break

        selected_calendar = [ c for c in results if c['summary'] == expected_name ]

        try:
            calendar = selected_calendar[0]
        except IndexError:
            raise Calendar.NotFoundError( f'no calendar named {expected_name!r} found' ) from None

        self.id = calendar['id']
        self.name = calendar['summary']
        self.timezone = timezone( calendar['timeZone'] )


    def __repr__( self ):
        return f'<Calendar name={self.name!r} id={self.id!r}>'


    def get_events( self, start, end ):
        if type( start ) is date:
            start = datetime.combine( start, START_OF_DAY )

        if type( end ) is date:
            end = datetime.combine( end, END_OF_DAY )

        start = self.timezone.localize( start )
        end = self.timezone.localize( end )

        results = []
        pageToken = None

        while True:
            this_results = Calendar.service.events().list(
                calendarId = self.id,
                timeMin = start.isoformat(),
                timeMax = end.isoformat(),
                singleEvents = True,
                orderBy = 'startTime',
                pageToken = pageToken,
            ).execute()

            results.extend( this_results.get( 'items', [] ) )

            try:
                pageToken = this_results['nextPageToken']
            except KeyError:
                break

        return [ Event( original = r, **r ) for r in results ]


    def add_event( self, start, summary, end = None, description = None, attachments = [] ):
        # we use type instead of isinstance because datetime is a subclass of date

        if end is None:
            if type( start ) is datetime:
                end = start + timedelta( minutes = 30 )
            elif type( start ) is date:
                end = start + ONE_DAY

        if type( start ) is datetime:
            if start.tzinfo is None:
                start = self.timezone.localize( start )
            start = dict( dateTime = start.isoformat() )
        elif type( start ) is date:
            start = dict( date = start.isoformat() )

        if type( end ) is datetime:
            if end.tzinfo is None:
                end = self.timezone.localize( end )
            end = dict( dateTime = end.isoformat() )
        elif type( end ) is date:
            end = dict( date = end.isoformat() )

        Calendar.service.events().insert(
            calendarId = self.id,
            body = dict(
                summary = summary,
                description = description,
                start = start,
                end = end,
                transparency = 'transparent',
                attachments = [ dict(
                    fileUrl = document.link,
                    mimeType = document.mime_type,
                    title = document.title,
                ) for document in attachments ],
            ),
            supportsAttachments = True,
        ).execute()



class Event:

    def __init__( self, start, end, summary = None, description = None, attachments = [], **unused ):
        if isinstance( start, dict ):
            if 'date' in start:
                start = date.fromisoformat( start['date'] )
            elif 'dateTime' in start:
                start = datetime.strptime( start['dateTime'], '%Y-%m-%dT%H:%M:%S%z' )
            else:
                raise ValueError( f'no "date" or "dateTime" found: {start!r}' )

        if isinstance( end, dict ):
            if 'date' in end:
                end = date.fromisoformat( end['date'] )
            elif 'dateTime' in end:
                end = datetime.strptime( end['dateTime'], '%Y-%m-%dT%H:%M:%S%z' )
            else:
                raise ValueError( f'no "date" or "dateTime" found: {end!r}' )

        self.summary = summary
        self.description = description
        self.start = start
        self.end = end
        self.is_all_day = type( start ) is date
        self.attachments = list()

        for attach in attachments:
            if attach['mimeType'] in ( 'application/vnd.google-apps.document', 'application/vnd.google-apps.kix' ):
                try:
                    self.attachments.append( DocumentKlass.lookup( attach['fileId'] ) )
                except Folder.NotFoundError:
                    continue
        #self.original = unused['original']


    def __repr__( self ):
        if self.is_all_day:
            return f'<Event start="{self.start:%Y-%m-%d}" summary={self.summary!r}>'
        else:
            return f'<Event start="{self.start:%Y-%m-%d %H:%M}" summary={self.summary!r}>'



class Folder:

    service = build( 'drive', 'v3', credentials = google_credentials, cache_discovery = False )

    class NotFoundError( Exception ):
        pass


    def __init__( self, name = None, id = None ):
        if name is None and id is None:
            raise ValueError( 'execpted either name or id, received neither' )
        elif name is not None and id is not None:
            folder = dict( name = name, id = id )
        elif name is not None:
            results = []
            pageToken = None

            while True:
                this_results = Folder.service.files().list(
                    q = f"mimeType = 'application/vnd.google-apps.folder' and name = '{name}'",
                    fields = "nextPageToken, files(id, name)",
                    pageToken = pageToken,
                ).execute()

                results.extend( this_results['files'] )

                try:
                    pageToken = this_results['nextPageToken']
                except KeyError:
                    break

            if len( results ) == 0:
                raise Folder.NotFoundError( f'no folder named {name!r} found' ) from None
            elif len( results ) > 1:
                print( f'WARNING: multiple folders named {name!r} found; picking the first' )

            folder = results[0]
        elif id is not None:
            try:
                folder = Folder.service.files().get(fileId = id, fields = "id, name" ).execute()
            except googleapiclient.errors.HttpError as err:
                raise Folder.NotFoundError( f'no folder with id {id!r} found' ) from None

        self.id = folder['id']
        self.name = folder['name']
        self._folders = None
        self._documents = None


    def __repr__( self ):
        return f'<Folder name={self.name!r} id={self.id!r}>'


    @property
    def folders( self ):
        if self._folders is None:
            results = []
            pageToken = None

            while True:
                this_results = Folder.service.files().list(
                    q = f"mimeType = 'application/vnd.google-apps.folder' and '{self.id}' in parents",
                    fields = "nextPageToken, files(id, name)",
                    pageToken = pageToken,
                ).execute()

                results.extend( this_results['files'] )

                try:
                    pageToken = this_results['nextPageToken']
                except KeyError:
                    break

            self._folders = { r['name']: Folder( id = r['id'], name = r['name'] ) for r in results }

        return self._folders


    @property
    def documents( self ):
        if self._documents is None:
            results = []
            pageToken = None

            while True:
                this_results = Folder.service.files().list(
                    q = f"( mimeType = 'application/vnd.google-apps.document' or mimeType = 'application/vnd.google-apps.kix' ) and '{self.id}' in parents",
                    fields = "nextPageToken, files(id, name, webViewLink)",
                    pageToken = pageToken,
                ).execute()

                results.extend( this_results['files'] )

                try:
                    pageToken = this_results['nextPageToken']
                except KeyError:
                    break

            self._documents = [ DocumentKlass( id = r['id'], title = r['name'], link = r['webViewLink'] ) for r in results ]

        return self._documents



class Document:

    @classmethod
    def lookup( cls, id ):
        try:
            document = Folder.service.files().get( fileId = id, fields = "id, name, webViewLink" ).execute()
        except googleapiclient.errors.HttpError as err:
            raise Folder.NotFoundError( f'no document with id {id!r} found' ) from None

        return DocumentKlass(
            id = document['id'],
            title = document['name'],
            link = document['webViewLink'],
        )


    def __init__( self, id = None, title = None, link = None ):
        self.id = id
        self.title = title
        self.link = link
        self.mime_type = 'application/vnd.google-apps.document'


    def __repr__( self ):
        return f'<Document title={self.title!r} id={self.id!r}>'


    def __eq__( self, other ):
        if type( self ) is not type( other ):
            return NotImplemented

        return self.id == other.id


    def __hash__( self ):
        return hash( self.id )



DocumentKlass = Document



__all__ = [ 'Calendar', 'Event', 'Folder', 'Document' ]

