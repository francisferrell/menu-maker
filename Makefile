
requirements.txt: requirements.in requirements.github.in
	$(eval VENV := $(shell mktemp --directory --tmpdir=. make-requirements.txt.XXXXXX))
	python3 -m venv "$(VENV)"
	"$(VENV)/bin/pip3" install -r requirements.in
	"$(VENV)/bin/pip3" uninstall --yes pkg-resources==0.0.0
	echo '# This file is auto-generated, do not edit it' >requirements.txt
	echo '# edit requirements.in' >>requirements.txt
	echo '# then run make requirements.txt' >>requirements.txt
	"$(VENV)/bin/pip3" freeze >>requirements.txt
	cat requirements.github.in >>requirements.txt
	rm -rf --one-file-system "$(VENV)"

venv/bin/make-list: requirements.txt requirements.dev.txt
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt
	./venv/bin/pip install -r requirements.dev.txt
	./venv/bin/pip install -e .

venv: venv/bin/make-list

shell: venv
	./venv/bin/python3

