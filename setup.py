
from pathlib import Path
from setuptools import setup
import sys

here = Path( __file__ ).parent.resolve()
sys.path.insert( 0, str( here ) )

import menumaker



setup(
    name = menumaker.__title__,
    version = '0.0.0' if menumaker.__version__ == 'git' else menumaker.__version__,
    description = menumaker.__description__,
    url = menumaker.__url__,
    author = menumaker.__author__,
    author_email = menumaker.__author_email__,
    license = menumaker.__license__,
    packages = [ 'menumaker' ],
    entry_points = {
        'console_scripts': [
            'make-list=menumaker.list:cli_main'
        ],
    }
)

