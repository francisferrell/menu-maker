
terraform {
  required_providers {
    archive = {
      source = "hashicorp/archive"
      version = "1.3.0"
    }
  }
}

provider "archive" {}



variable "name" {
  type = string
  description = "the lambda name"
}

variable "google_api_credentials_arn" {
  type = string
  description = "ARN of the secret holding the Google API credentials"
}

variable "google_api_credentials_read_policy_arn" {
  type = string
  description = "ARN of an IAM policy granting read access ot the google credentials secret"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "lambda_name" {
  description = "the name of the lambda"
  value = aws_lambda_function.function.function_name
}

output "lambda_arn" {
  description = "the ARN of the lambda"
  value = aws_lambda_function.function.arn
}

output "deploy_policy_arn" {
  description = "the IAM policy ARN granting permission to deploy new code"
  value = aws_iam_policy.deploy_policy.arn
}



resource "aws_cloudwatch_log_group" "logs" {
  name = "/aws/lambda/${var.name}"
  retention_in_days = 14
  tags = var.tags
}



data "aws_iam_policy_document" "execution_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "execution_role" {
  name = "${var.name}-execution-role"
  description = "grants permissions to the ${var.name} lambda"
  assume_role_policy = data.aws_iam_policy_document.execution_policy_document.json

  tags = var.tags
}



data "aws_iam_policy_document" "logging_policy_document" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "${aws_cloudwatch_log_group.logs.arn}:*",
    ]
  }
}

resource "aws_iam_policy" "logging_policy" {
  name = "${var.name}-logging"
  description = "grants permission for ${var.name} lambda to write logs"
  policy = data.aws_iam_policy_document.logging_policy_document.json
  tags = var.tags
}

resource "aws_iam_role_policy_attachment" "logging_attachment" {
  role = aws_iam_role.execution_role.name
  policy_arn = aws_iam_policy.logging_policy.arn
}

resource "aws_iam_role_policy_attachment" "google_credentials_attachment" {
  role = aws_iam_role.execution_role.name
  policy_arn = var.google_api_credentials_read_policy_arn
}



data "archive_file" "dummy_zip" {
  type = "zip"
  output_path = "${path.module}/dummy.zip"

  source {
    content = "def lambda_handler(event,context):\n  pass"
    filename = "menumaker/menu.py"
  }
}



resource "aws_lambda_function" "function" {
  function_name = var.name
  role = aws_iam_role.execution_role.arn
  filename = data.archive_file.dummy_zip.output_path

  runtime = "python3.8"
  handler = "menumaker.menu.lambda_handler"
  timeout = 60

  environment {
    variables = {
      GOOGLE_API_CREDENTIALS_ARN = var.google_api_credentials_arn
    }
  }

  tags = var.tags
}



data "aws_iam_policy_document" "deploy_policy_document" {
  statement {
    actions = [
      "lambda:UpdateFunctionCode",
    ]
    resources = [
      aws_lambda_function.function.arn
    ]
  }
}

resource "aws_iam_policy" "deploy_policy" {
  name = "${var.name}-deploy"
  description = "grants permission to deploy new code to ${var.name} lambda"
  policy = data.aws_iam_policy_document.deploy_policy_document.json
  tags = var.tags
}

