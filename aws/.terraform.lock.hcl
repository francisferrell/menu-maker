# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/archive" {
  version     = "1.3.0"
  constraints = "1.3.0"
  hashes = [
    "h1:bzjSdD6pqVQBmcfi+Muz2rgHVDBUZNqSqyDIHbN14zI=",
    "zh:115aa6bc7825402a8d4e2e954378a9f48e4fdbeabe081ffd04e0a2f6786159bb",
    "zh:21f731ffac20a67615c64a7a8a96949c971ee28ffd5807d8c299faba73b5e273",
    "zh:2e81b58e141b175cbf801ade5e87c5db4cb28933216b0547ef32c95500385904",
    "zh:3acbb96fd142b4d193dc18861340281249301368029169e346d15410d0572492",
    "zh:4346edee0dfe97154b6f28d9ef0fa762131db92b78bbd1b3207945201cb59818",
    "zh:93916a84cc6ff6778456dd170a657326c4dd3a86b4434e424a66a87c2535b888",
    "zh:ade675c3ac8b9ec91131bac5881fbd4efad46a3683f2fea2efb9493a2c1b9ffb",
    "zh:b0a0cb13fc850903aa7a057ae7e06366939b8f347926dce1137cd47b9123ad93",
    "zh:d6d838cceffb7f3ff27fb9b51d78fccdef15bd32408f33a726556bfe66315bd3",
    "zh:ddc4ac6aea6537f8096ffeb8ff3bca355f0972793184e0f6df120aa6460b4446",
    "zh:e0d1213625d40d124bd9570f0d92907416f8d61bc8c389c776e72c0a97020cce",
    "zh:eb707b69f9093b97d98e2dece9822852a27849dd1627d35302e8d6b9801407ef",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.64.0"
  constraints = "3.64.0"
  hashes = [
    "h1:SIyCcDYMwX3UDB/HCTXAsRE1ZPEI1+wnR9ur9l+1VDg=",
    "zh:2538efea7190e87ef78c644b53673aa6c8a6dd641f0e8ba98ad6c0b7f11fe17c",
    "zh:397b80d52a8a8ac6b4d2b6848e26c87298789aba8f5d4458ffa855553956f3ec",
    "zh:572987afda6190fc56fce0c327960e163850a1ed2cdc8b3f0a28d131d647a30b",
    "zh:78ff10f69f6410a665e28550f4728b94c323b48894ee19c4443fe77f7dcc6bc3",
    "zh:8da9d1dc247cadc5bf334735286462eb8355ce9a218238734dbad6c0e19dd70d",
    "zh:8ef547145b504c11d30bd7e3cfa37bf688fe3a8f0fd791883c2e1d6dc2398469",
    "zh:9a8d84ddc25e9652e5c5f4827447eec6d039a70a26d35a7b044979b6fc31f600",
    "zh:9f487b220897b1dcef3d67db37129447fe2975de132d74b9fed0042e8528af59",
    "zh:c23cd3976cb7e6e49c0d0675f503c36aa5b941037155c7c8b6e33462863da41e",
    "zh:ddf99c227c2a0e207b61441fa4f603920a741d75a2de884de512e94344e3c2e9",
    "zh:f8b8421971b798740410cbea11ffe7b7d685a1b0848208abc0fb59fcc933b42a",
  ]
}
