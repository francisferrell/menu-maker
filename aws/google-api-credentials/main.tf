
variable "name" {
  type = string
  description = "the name of the secret"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "secret_arn" {
  description = "the ARN of the secret"
  value = aws_secretsmanager_secret.secret.arn
}

output "read_policy_arn" {
  description = "the IAM policy ARN granting read access to the secret"
  value = aws_iam_policy.read_policy.arn
}



resource "aws_secretsmanager_secret" "secret" {
  name = var.name
  description = "Google API service account credentials"
  recovery_window_in_days = 0
  tags = var.tags
}



data "aws_iam_policy_document" "read_access" {
  statement {
    actions = [
      "secretsmanager:GetSecretValue",
    ]

    resources = [
      aws_secretsmanager_secret.secret.arn
    ]
  }
}

resource "aws_iam_policy" "read_policy" {
  name = "${var.name}-read-access"
  description = "grants access to read the secret for ${var.name}"
  policy = data.aws_iam_policy_document.read_access.json
  tags = var.tags
}

