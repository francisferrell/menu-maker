
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.64.0"
    }
  }

  backend "s3" {
    profile = "terraform"
    region = "us-east-1"
    bucket = "terraform-francisferrell"
    key = "menu-maker.tfstate"
    dynamodb_table = "terraform-francisferrell"
  }
}

provider "aws" {
  profile = "terraform"
  region = "us-east-1"
}



variable "name" {
  type = string
  description = "name of the application"
}

variable "menu_maker_cron_spec" {
  type = string
  description = "schedule for running the menu-maker lambda in AWS-compatible cron format"
}

variable "list_maker_cron_spec" {
  type = string
  description = "schedule for running the list-maker lambda in AWS-compatible cron format"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt IAM secret access keys"
  default = null
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
  default = {}
}



output "gitlab_access_key_id" {
  description = "the gitlab user's access key id"
  value = module.gitlab-deploy-user.access_key_id
}

output "gitlab_secret_access_key" {
  description = "the gitlab user's access key id"
  value = module.gitlab-deploy-user.secret_access_key
}

output "google_api_credentials_arn" {
  description = "the ARN of the google API service account credentials secret"
  value = module.google-api-credentials.secret_arn
}



module "google-api-credentials" {
  source = "./google-api-credentials"

  name = "${var.name}-google-api-credentials"
  tags = var.tags
}



data "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-francisferrell"
}

data "aws_iam_policy_document" "terraform_state_access" {
  statement {
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${data.aws_s3_bucket.terraform_state.arn}/menu-maker.tfstate",
    ]
  }
}

resource "aws_iam_policy" "terraform_state_policy" {
  name = "${data.aws_s3_bucket.terraform_state.id}-menu-maker.tfstate-read-access"
  description = "grants read access to s3://${data.aws_s3_bucket.terraform_state.id}/menu-maker.tfstate"
  policy = data.aws_iam_policy_document.terraform_state_access.json
  tags = var.tags
}

module "gitlab-deploy-user" {
  source = "./api-user"

  name = "gitlab-${var.name}"
  keybase_user = var.keybase_user
  policy_arns = [
    aws_iam_policy.terraform_state_policy.arn,
    module.google-api-credentials.read_policy_arn,
    module.lambda.deploy_policy_arn,
    module.list-lambda.deploy_policy_arn,
  ]
  tags = var.tags
}



module "lambda" {
  source = "./lambda"
  name = var.name
  google_api_credentials_arn = module.google-api-credentials.secret_arn
  google_api_credentials_read_policy_arn = module.google-api-credentials.read_policy_arn
  tags = var.tags
}



module "list-lambda" {
  source = "./list-lambda"
  name = "list-maker"
  google_api_credentials_arn = module.google-api-credentials.secret_arn
  google_api_credentials_read_policy_arn = module.google-api-credentials.read_policy_arn
  tags = var.tags
}



module "menu-maker-schedule" {
  source = "./schedule"
  name = "${var.name}-schedule"
  cron_spec = var.menu_maker_cron_spec
  lambda_name = module.lambda.lambda_name
  lambda_arn = module.lambda.lambda_arn
  input_json = "{}"
  tags = var.tags
}



module "list-maker-schedule" {
  source = "./schedule"
  name = "list-maker-schedule"
  cron_spec = var.list_maker_cron_spec
  lambda_name = module.list-lambda.lambda_name
  lambda_arn = module.list-lambda.lambda_arn
  input_json = "{}"
  tags = var.tags
}

