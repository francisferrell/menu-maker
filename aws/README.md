
# AWS

This terraform project creates:

* an IAM user for use in gitlab CI
* a secret to store the google API credentials

## Inputs

Optional inputs are:

* `keybase_user` -- to receive the IAM user's secret key securely as an output
* `tags` -- to tag things

## Outputs

The following values are output by terraform

* `gitlab_access_key_id` -- credentials for the gitlab IAM user
* `gitlab_secret_access_key` -- credentials for the gitlab IAM user
* `google_api_credentials_arn` -- ARN of a secrets manager entry created to store your
  google API service account's credentials

Note that you'll have to populate the google credentials secret manually.

## Known Limitations

Due to a limitation of terraform, the first `terraform apply` will fail with an error
about an invalid `for_each` related to the `gitlab-deploy-user` module. This is because
the IAM roles each other module will create don't exist yet to be attached to the IAM
user. The `policy_arns` in `main.tf` must be commented out for the first apply, or the
terraform CLI option `-target` must be utilized to first apply everything except the
`gitlab-deploy-user` module.

