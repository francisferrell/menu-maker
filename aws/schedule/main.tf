
variable "name" {
  type = string
  description = "the schedule name"
}

variable "cron_spec" {
  type = string
  description = "the schedule cron spec"
}

variable "lambda_name" {
  type = string
  description = "the name of the lambda"
}

variable "lambda_arn" {
  type = string
  description = "the ARN of the lambda"
}

variable "input_json" {
  type = string
  default = null
  description = "argument to pass to the lambda"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



resource "aws_cloudwatch_event_rule" "schedule" {
  name = var.name
  description = "schedule the menu every week"
  schedule_expression = "cron(${var.cron_spec})"

  tags = var.tags
}



resource "aws_cloudwatch_event_target" "target" {
  rule = aws_cloudwatch_event_rule.schedule.name
  target_id = "${var.name}-target"
  arn = var.lambda_arn
  input = var.input_json
}



resource "aws_lambda_permission" "allow_cloudwatch_to_call_check_foo" {
  action = "lambda:InvokeFunction"
  function_name = var.lambda_name
  principal = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.schedule.arn
}

